
/* Ejercicio 1
Considere el famoso problema de reordenar una lista de n números enteros. Escriba un  programa que realice la reordenación de modo que no se utilice almacenamiento innecesario. Esto es, el programa contendrá un solo arreglo unidimensional de enteros llamado x en el que se reordenarán los elementos de uno en uno. Utilice el reordenamiento del tipo burbuja.
El programa será del tipo conversacional (interactivo) donde se podrá realizar cualquiera de las siguientes operaciones:
Ordenamiento de menor a mayor en valor absoluto.
Ordenamiento de menor a mayor algebraicamente (con signo).
Ordenamiento de mayor a menor en valor absoluto.
Ordenamiento de mayor a menor algebraicamente (con signo).
Tip: Utilice los siguientes valores como arreglo de testeo: 
[4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7]
 */

// C program for implementation of Bubble sort
// inspirado en https://www.geeksforgeeks.org/bubble-sort/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

struct data
{
    char nombreEquipo[20];
    int nroVictorias;
    int nroDerrotas;
    int nroEmpates;
    int nroGoles;
};

//cabeceras
//interfaz
void mostrarElementos(size_t dbSize);
void mostrarDatabase();
void mainMenu(void);
void printArray();
//gestion de datos
void resizeArray(size_t dbSize);
int compararVictorias(const void *_a, const void *_b);
int compararDerrotas(const void *_a, const void *_b);
int compararGoles(const void *_a, const void *_b);
//clasificadores
void mayorAMenorAlg(int arr[], int size);
void menorAMayorAlg(int arr[], int size);

int salir = 0;
int i, eleccionUsuario;
int dbSize = 0;
struct data *struct_array;
int arr[] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};

// gestion de datos
void resizeArray(size_t dbSize)
{
    //TODO: Handle reallocations errors
    struct_array = realloc(struct_array, dbSize * sizeof *struct_array);
}
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// clasificadores implementando bubblesort
void mayorAMenorAlg(int arr[], int size)
{
    int i, j;
    for (i = 0; i < size - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < size - i - 1; j++)
            if (arr[j] < arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}
void mayorAMenorAbs(int arr[], int size)
{
    int i, j;
    for (i = 0; i < size - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < size - i - 1; j++)
            if (abs(arr[j]) < abs(arr[j + 1]))
                swap(&arr[j], &arr[j + 1]);
}
void menorAMayorAlg(int arr[], int size)
{
    int i, j;
    for (i = 0; i < size - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < size - i - 1; j++)
            if (arr[j] > arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}
void menorAMayorAbs(int arr[], int size)
{
    int i, j;
    for (i = 0; i < size - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < size - i - 1; j++)
            if (abs(arr[j]) > abs(arr[j + 1]))
                swap(&arr[j], &arr[j + 1]);
}

// interfaz de usuario
void printArray()
{
    int i;
    for (i = 0; i < dbSize; i++)
        printf("%d ", arr[i]);
    printf("\n");
}
void mainMenu(void)
{
    printf("Please select from the following options:\n");
    printf("1: crear database con la lista de numeros\n");
    printf("2: Display database contents\n");
    printf("3: exit the program\n");
    printf("4: ordenar menor a mayor segun v. absoluto\n");
    printf("5: ordenar menor a mayor algebraicamente (con signo)\n");
    printf("6: ordenar mayor a menor en v. absoluto\n");
    printf("7: ordenar mayor a menor algebraicamente (con signo)\n");
    scanf("%d", &eleccionUsuario);

    switch (eleccionUsuario)
    {
    case 1:
        dbSize = sizeof(arr) / sizeof(arr[0]);
        printf("original array: \n");
        printArray();
        break;
    case 2:
        mostrarDatabase();
        break;
    case 3:
        printf("salir\n\n");
        salir = 1;
        break;
    case 4:
        printf("4: ordenar menor a mayor segun v. absoluto\n");
        menorAMayorAbs(arr, dbSize);
        mostrarDatabase();
        break;
    case 5:
        printf("5: ordenar menor a mayor algebraicamente (con signo)\n");
        menorAMayorAlg(arr, dbSize);
        mostrarDatabase();
        break;
    case 6:
        printf("6: ordenar mayor a menor en v. absoluto\n");
        mayorAMenorAbs(arr, dbSize);
        mostrarDatabase();
        break;
    case 7:
        printf("7: ordenar mayor a menor algebraicamente (con signo)\n");
        mayorAMenorAlg(arr, dbSize);
        mostrarDatabase();
        break;
            default:
        printf("Error: opcion equivocada\n");
        break;
    }
}
/*void mostrarElementos(size_t dbSize)
{
    size_t i;
    for (i = 0; i < dbSize; i++)
    {
        printf("%s ", struct_array[i].nombreEquipo);
        printf("\t%d ", struct_array[i].nroVictorias);
        printf("\t%d ", struct_array[i].nroDerrotas);
        printf("\t%d ", struct_array[i].nroEmpates);
        printf("\t%d ", struct_array[i].nroGoles);
        printf("\n");
    }
}*/
void mostrarDatabase()
{
    printf("\n---------------Display content:-------------------\n");
    printArray();
    printf("----------------end of content------------------\n");
}

int main()
{

    struct_array = malloc(2 * sizeof(int));
    while (!salir)
    {
        mainMenu();
    }
    free(struct_array);
    return 0;
}