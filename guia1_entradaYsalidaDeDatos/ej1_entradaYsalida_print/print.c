/*
Escribir una función que utilice printf para cada una de las siguientes situaciones, suponiendo que todos los enteros se desean presentar como cantidades decimales.
Escribir los valores de i, j, x y dx suponiendo que cada cantidad entera tiene una longitud de campo mínima de cuatro caracteres y cada cantidad en coma flotante se presenta en notación exponencial con un total de al menos 14 caracteres y no más de 8 cifras decimales.
Repetir A, visualizando cada cantidad en una línea.
Escribir los valores de i, ix, j, x y u suponiendo que cada cantidad entera tendrá una longitud de campo mínima de cinco caracteres, el entero largo tendrá una longitud de campo mínima de 12 caracteres y la cantidad en coma flotante tiene al menos 10 caracteres con un máximo de cinco cifras decimales. No incluir el exponente.
Repetir C, visualizando las tres primeras cantidades en una línea, seguidas de una línea en blanco y las otras dos cantidades en la línea siguiente.
Escribir los valores de i, u y c, con una longitud de campo mínima de seis caracteres para cada cantidad entera. Separar con tres espacios en blanco cada cantidad.
Escribir los valores de j, u y x. Visualizar las tres cantidades enteras con una longitud de campo mínima de cinco caracteres. Presentar la cantidad en coma flotante utilizando la conversión tipo f, con una longitud mínima de 11 caracteres y un máximo de cuatro cifras decimales.
Repetir F, con cada dato ajustado a la izquierda dentro de su campo.
Repetir F, apareciendo un signo (tanto + como -) delante de cada dato con signo.
Repetir F, rellenando el campo de cada entidad entera con ceros.
Repetir F, apareciendo el valor de x con un punto decimal.
*/

#include <stdint.h>
#include <stdio.h>

int main()
{
    int i = 102;
    int j = -56;
    long ix = -158693157400;
    unsigned u = 35460;
    float x = 12.687;
    double dx = 0.000000025;
    char c = 'C';

    // A
    printf("%4d ", i);
    printf("%4d ", j);
    printf("%14.8e ", x);
    printf("%14.8e \n\n", dx);
    printf("\n--------------------\n");

    // B
    printf("%4d \n", i);
    printf("%4d \n", j);
    printf("%14.8e \n", x);
    printf("%14.8e \n\n", dx);
    printf("\n--------------------\n");

    // C
    printf("%5d \n", i);
    printf("%4d \n", j);
    printf("%12ld \n", ix);
    printf("%10.5f \n", x);
    printf("%5u \n\n", u);
    printf("\n--------------------\n");

    // D
    printf("%5d ", i);
    printf("%4d ", j);
    printf("%12ld \n\n", ix);
    printf("%10.5f ", x);
    printf("%5u ", u);
    printf("\n\n");
    printf("\n--------------------\n");

    // E
    printf("%6d   ", i);
    printf("%6u   ", u);
    printf("%6c", c);
    printf("\n\n");
    printf("\n--------------------\n");

    // F
    printf("%5d ", j);
    printf("%5u ", u);
    printf("%11.4f ", x);
    printf("\n\n");
    printf("\n--------------------\n");

    // G
    printf("%-5d ", j);
    printf("%-5u ", u);
    printf("%-11.4f ", x);
    printf("\n\n");
    printf("\n--------------------\n");

    // H
    printf("%+5d ", j);
    printf("%+5u ", u);
    printf("%+11.4f ", x);
    printf("\n\n");
    printf("\n--------------------\n");

    // I
    printf("%05d ", j);
    printf("%05u ", u);
    printf("%011.4f ", x);
    printf("\n\n");
    printf("\n--------------------\n");

    // J
    printf("%5d ", j);
    printf("%5u ", u);
    printf("%#11.4f ", x);
    printf("\n\n");
    printf("\n--------------------\n");

    return 0;
}