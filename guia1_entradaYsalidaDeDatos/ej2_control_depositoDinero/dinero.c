
/*
Escriba un programa en C de estilo conversacional que determine lo siguiente:
¿Cuánto dinero se acumulara después de 30 años si se depositan 100 dólares al comienzo de cada año y el tipo de interés compuesto es el 6% anual?
¿Cuánto dinero se debe depositar al comienzo de cada año para acumular 100.000 dólares después de 30 años, suponiendo también un tipo de interés compuesto del 6% anual?
En cada caso determinar primero la cantidad de dinero desconocida. Luego crear una tabla mostrando la cantidad total de dinero que se acumulara al final de cada año. Definir una función para la potenciación. 

*/

#include <stdio.h>

double potenciacion(int exponente, float interes)
{
    double resultado = 1;
    int i = 1;
    double potenciacion = (1 + interes / 100);
    while (i <= exponente)
    {
        resultado = resultado * potenciacion;
        i++;
    }
    return potenciacion;
}

int main()
{
    // 1
    float A = 100;
    int n = 30;
    float i = 0.06;
    double F = 0;
    double aux = 0;
    printf("\n 1. cantidad de dinero inicial:\n ");
    scanf("%lf", &F);

    for (int j = 1; j <= n; j++)
    {
        aux = aux + A * (potenciacion(j, i));
        printf("Dinero acumulado en %d años: %lf \n", j, aux);
    }
    F = F + aux;
    printf("total acumulado = %lf", F);

    // 2

    printf("\n 2. cantidad de dinero inicial:\n ");
    scanf("%lf", &F);
    i = 0.06;
    F = 100000 - F;
    aux = 0;
    n = 30;
    for (int j = 1; j <= n; j++)
    {
        aux = aux + (potenciacion(j, i));
    }
    printf("cant. dinero : %f \n", aux);
    A = F / aux;
    printf("Cantidad de dinero necesario a agregar por año: %f \n", A);
    return 0;
}