#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int words(const char sentence[])
{
    //inspirado en https://stackoverflow.com/questions/12698836/counting-words-in-a-string-c-programming
    int counted = 0; // result

    // state:
    const char *it = sentence;
    int inword = 0;

    do
        switch (*it)
        {
        case '\0':
        case ' ':
        case '\t':
        case '\n':
        case '\r': // TODO others?
            if (inword)
            {
                inword = 0;
                counted++;
            }
            break;
        default:
            inword = 1;
        }
    while (*it++);

    return counted;
}

int main()
{
    printf("contar \n");
    FILE *fp;
    fp = fopen("file.txt", "rb"); // Open file for reading
    fseek(fp, 0, SEEK_END);
    long fsize = ftell(fp);
    printf("tamanio del archivo abierto es fsize: %ld \n", fsize);
    fseek(fp, 0, SEEK_SET);
    char *string = malloc(fsize + 1);
    fread(string, 1, fsize, fp);
    fclose(fp);
    string[fsize] = 0;
    /*
    printf("\n\n\nel contenido del archivo es: \n\n\n %s\n", string);

    // contar palabras
    printf("%d\n", words(""));
    printf("%d\n", words("\t"));
    printf("%d\n", words("   a      castle     "));
    printf("%d\n", words("my world is a castle"));
    */

    printf("las palabras encontradas en el archivo de texto son: \n");
    printf("%d\n", words(string));
    return (0);
}