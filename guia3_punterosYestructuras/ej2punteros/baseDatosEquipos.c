/*Ejercicio 2
Escribir un programa interactivo que acepte la siguiente información sobre equipos de la liga de fútbol, para el torneo actual:
Nombre del equipo.
Número de victorias.
Número de derrotas.
Número de empates.
Cantidad de goles.
Introducir esa información para todos los equipos deseados. Permitir al usuario ver el listado ordenado de equipos según:
Victorias
Derrotas
Cantidad de goles.
La información para cada equipo estará almacenada en un arreglo almacenado dinámicamente en memoria. La/s función/es de ordenamiento utilizaran almacenamiento dinámico el cual será liberado luego de retornar la información solicitada.
*/

// inspirado en https://stackoverflow.com/questions/12990723/dynamic-array-of-structs-in-c
// ordenamiento http://arantxa.ii.uam.es/~swerc/ejemplos/csorting.html

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

int salir = 0;
int i, n, p;
struct data *struct_array;

struct data
{
    char nombreEquipo[20];
    int nroVictorias;
    int nroDerrotas;
    int nroEmpates;
    int nroGoles;
};

struct data get_data(void)
{
    //TODO: Try to avoid passing your structure (40 bytes + padding) without pointer
    struct data thisdata;
    char auxChar[20];
    int auxInt = 0;
    printf("ingrese nombre del equipo: \n");
    scanf("%s", thisdata.nombreEquipo); // TODO: Avoid using `scanf` for human inputs

    printf("ingrese numero de victorias\n");
    scanf("%s", auxChar);
    sscanf(auxChar, "%d", &thisdata.nroVictorias); //convertir el arreglo de char en un solo numero entero

    printf("ingrese numero de derrotas\n");
    scanf("%s", auxChar);
    sscanf(auxChar, "%d", &thisdata.nroDerrotas);

    printf("ingrese numero de empates\n");
    scanf("%s", auxChar);
    sscanf(auxChar, "%d", &thisdata.nroEmpates);

    printf("ingrese numero de goles\n");
    scanf("%s", auxChar);
    sscanf(auxChar, "%d", &thisdata.nroGoles);

    return thisdata;
}

void mostrarElemento(size_t n)
{
    size_t i;
    for (i = 0; i < n; i++)
    {
        printf("%s ", struct_array[i].nombreEquipo);
        printf("\t%d ", struct_array[i].nroVictorias);
        printf("\t%d ", struct_array[i].nroDerrotas);
        printf("\t%d ", struct_array[i].nroEmpates);
        printf("\t%d ", struct_array[i].nroGoles);
        printf("\n");
    }
}

void mostrarDatabase()
{
    printf("Display content:\n");
    printf("nom");
    printf("\tVic");
    printf("\tDer");
    printf("\tEmp");
    printf("\tGol");
    printf("\n");
    mostrarElemento(n);
    printf("\nend of content\n");
}

void resizeArray(size_t n)
{
    //TODO: Handle reallocations errors
    struct_array = realloc(struct_array, n * sizeof *struct_array);
}

int compararVictorias(const void *_a, const void *_b)
{
    struct data *a, *b;
    a = (struct data *)_a;
    b = (struct data *)_b;
    int aux = (a->nroVictorias - b->nroVictorias);
    return aux;
}

int compararDerrotas(const void *_a, const void *_b)
{
    struct data *a, *b;
    a = (struct data *)_a;
    b = (struct data *)_b;
    int aux = (a->nroDerrotas - b->nroDerrotas);
    return aux;
}

int compararGoles(const void *_a, const void *_b)
{
    struct data *a, *b;
    a = (struct data *)_a;
    b = (struct data *)_b;
    int aux = (a->nroGoles - b->nroGoles);
    return aux;
}

void mainMenu(void)
{

    printf("Please select from the following options:\n");
    printf("1: crear database y agregar equipos\n");
    printf("2: Display database contents\n");
    printf("3: exit the program\n");
    printf("4: ordenar segun victorias\n");
    printf("5: ordenar segun derrotas\n");
    printf("6: ordenar segun cantidad de goles\n");
    scanf("%d", &p);

    switch (p)
    {
    case 1:
        printf("ingresar el nro de equipos a registrar:\n");
        scanf("%u", &n);
        resizeArray(n);
        for (i = 0; i < n; i++)
        {
            struct_array[i] = get_data();
        }
        break;
    case 2:
        mostrarDatabase();
        break;
    case 3:
        printf("salir\n\n");
        salir = 1;
        break;
    case 4:
        printf("ordenar segun victorias\n");
        qsort(struct_array, n, sizeof(struct data), compararVictorias);
        mostrarDatabase();
        break;
    case 5:
        printf("ordenar segun derrotas\n");
        qsort(struct_array, n, sizeof(struct data), compararDerrotas);
        mostrarDatabase();
        break;
    case 6:
        printf("ordenar segun cant goles\n");
        qsort(struct_array, n, sizeof(struct data), compararGoles);
        mostrarDatabase();
        break;
    }
}

int main(void)
{
    struct_array = malloc(2 * sizeof(int));
    while (!salir)
    {
        mainMenu();
    }
    free(struct_array);
    return 0;
}
